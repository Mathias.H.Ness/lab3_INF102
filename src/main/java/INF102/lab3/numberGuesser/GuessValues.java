package INF102.lab3.numberGuesser;

public record GuessValues(int lowerHigher, int value) {}
