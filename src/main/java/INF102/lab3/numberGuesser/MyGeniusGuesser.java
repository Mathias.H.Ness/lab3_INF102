package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {
    private int lowerBound;
	private int upperBound;
    private GuessValues guess;

	@Override
    public int findNumber(RandomNumber number) {
        lowerBound = number.getLowerbound();
		upperBound = number.getUpperbound();
        guess = makeGuess(lowerBound, upperBound, number);

        while (guess.lowerHigher() != 0) {
            if (guess.lowerHigher() == -1) {
                lowerBound = guess.value();
                guess = makeGuess(lowerBound, upperBound, number);
            } else {
                upperBound = guess.value();
                guess = makeGuess(lowerBound, upperBound, number);
            }
        }
        return guess.value();
    }

    private GuessValues makeGuess(int lowerBound, int upperBound, RandomNumber number) {
        int guess = (upperBound + lowerBound)/2;
        return new GuessValues(number.guess(guess), guess);
    }


}
